# Note: This is a host entry in my ~/.ssh/config.
# Adjust as needed; ask in the channel if you need the detail.
DEST=polarisml:/var/www_polarisml/.

# Note: leaving out the "--delete" option, which would delete anything
# on the server that doesn't match what's in our local copy.

# Also note: there are no brakes on this!  Do a test-publish first if
# you're not sure!
.PHONY: publish
publish: build
	rsync -avr public/. $(DEST)

.PHONY: build
build:
	hugo

.PHONY: test-publish
test-publish:
	rsync -avr --dry-run public/. $(DEST)
